{
  networking = {
    hostName = "container2"; domain = "infra.ix-api.net";
    
    # Private network interface
    interfaces.ens7 = {
       mtu = 1450;
       ipv4.addresses = [{address = "10.0.0.4"; prefixLength = 16; }];
    };
    firewall.trustedInterfaces = ["ens7"];
  };
  
  users.motd = builtins.readFile ./motd;


  machine = {
    # Role: container host:
    container-host.enable = true;
    container-host = {
      website      = { enable = true; listen = "10.0.0.4:9001"; };
      docs.live    = { enable = true; listen = "10.0.0.4:8001"; };
      docs.staging = { enable = true; listen = "10.0.0.4:8002"; };
    };

    # Configure monitoring and enable collector
    # on this host.
    monitoring = {
      collector.enable = true;
      collector.listen = "10.0.0.4:8428";

      metrics.enable    = true;
      metrics.collector = "http://10.0.0.4:8428";

      webmon.enable = true; # Activ collection of website data

      grafana = {
        enable = true;
        addr   = "10.0.0.4"; port = 7200;
      };
    };
  };

}
