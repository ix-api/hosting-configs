{ config, pkgs, ... }:
let
  vpcAddr4 = "10.0.0.200";
in
{
  config = {
    networking = {
      hostName = "service1"; domain = "infra.ix-api.net"; 
    };
  
    # This machine is a service host 
    machine = {
      vpc = {
        enable = true;
        interface = "ens9";
        addr4 = vpcAddr4;
      };

      monitoring = {
        collector.enable = true;
        collector.listen = "${vpcAddr4}:8428";

        metrics.enable = true;
        metrics.collector = "http://${vpcAddr4}:8428";

        webmon.enable = true;

        # This will also be the grafana host
        grafana = {
          enable = true;
          addr   = vpcAddr4;
          port   = 7200;
        };
      };
        
      # Mailing lists and forward mailboxes
      mailer.enable = true;
    };
  
    users.motd = builtins.readFile ./motd;
  };
}
