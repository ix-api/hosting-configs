{
  networking = {
    hostName = "container1"; domain = "infra.ix-api.net";
    
    interfaces.ens7 = {
      mtu = 1450;
      ipv4.addresses = [ {address = "10.0.0.3"; prefixLength = 16;} ];
    };
    
    firewall.trustedInterfaces = [ "ens7" ];
  };


  # This machine is a container host
  machine = {
    container-host.enable = true;
    container-host = {
      # Docs
      docs.live.enable = true;
      docs.live.listen = "10.0.0.3:8001";

      docs.staging.enable = true;
      docs.staging.listen = "10.0.0.3:8002";

      # Website
      website.enable = true;
      website.listen = "10.0.0.3:9001";
    };

    monitoring.metrics.enable    = true;
    monitoring.metrics.collector = "http://10.0.0.4:8428";
  };

  users.motd = builtins.readFile ./motd;
}
