{ config, pkgs, ... }:
let
  vpcAddr4 = "10.0.0.100";
in
{
  config = {
    networking = {
      hostName = "web1"; domain = "infra.ix-api.net"; 

      # Public interface
      interfaces.ens3 = {
        useDHCP = true;
        ipv6 = {
          addresses = [
            { address = "2a05:f480:1400:00d5:5400:04ff:feb1:6630";
              prefixLength = 64; }
            { address = "2a05:f480:1400:00d5::1";
              prefixLength = 64; }
          ];
        };
      };
    };

    # This machine is the primary web host
    machine = {
      vpc = {
        enable = true;
        interface = "ens9";
        addr4 = vpcAddr4;
      };

      # Metric collection
      monitoring = {
        metrics.enable    = true;
        metrics.collector = "http://10.0.0.200:8428";
      };

      # Containers
      container-host.enable = true;
      container-host = {
        website      = { enable = true; listen = "127.0.0.1:9001"; };
        docs.live    = { enable = true; listen = "127.0.0.1:8001"; };
        docs.staging = { enable = true; listen = "127.0.0.1:8002"; };
        docs.preview = { enable = true; listen = "127.0.0.1:8020"; };
        
        sandbox = {
           enable = true;
           fqdn = "sandbox.ix-api.net";
           listen = "127.0.0.1:8030";
        };
      };

      load-balancer.enable = true;
      load-balancer.vhosts = {
        # Public Website
        "www.ix-api.net".upstreams = [
            "127.0.0.1:9001"
        ];
        "ix-api.net".upstreams = [
            "127.0.0.1:9001"
        ];

        "sandbox.ix-api.net".upstreams = [ "127.0.0.1:8030" ];
  
        # Docs (live and staging)
        "docs.ix-api.net".upstreams = [
            "127.0.0.1:8001"
        ];
  
        "docs-staging.ix-api.net".upstreams = [
            "127.0.0.1:8002"
        ];

        "docs-preview.ix-api.net".upstreams = [
            "127.0.0.1:8020"
        ];

  
        # Monitoring
        "grafana.infra.ix-api.net".upstreams = [
            "10.0.0.200:7200" 
        ];
      };
    };

    # Additional VHost config, for errors.ix-api.net
    services.nginx.virtualHosts = {
      "errors.ix-api.net" = {
        enableACME = true;
        forceSSL   = true;
  
        # This is using the same upstream as docs,
        # however we differ in redirecting and proxies
        locations = {
          "/"    = { return = "302 /v2/"; };
          "/v2/" = {
              proxyPass = "http://lb-docs-staging.ix-api.net";
              proxyWebsockets = true;
              extraConfig = ''
                rewrite /v2/(.*) /v2/problems/$1 break;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host;
                proxy_set_header X-Forwarded-Proto $scheme;
              '';
          };
          "/v1/" = {
              proxyPass = "http://lb-docs-staging.ix-api.net";
              proxyWebsockets = true;
              extraConfig = ''
                rewrite /v1/(.*) /v1/problems/$1 break;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host;
                proxy_set_header X-Forwarded-Proto $scheme;
              '';
          };
        };
      };
    };
  
    users.motd = builtins.readFile ./motd;
  };
}
