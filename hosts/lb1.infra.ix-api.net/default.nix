{
  networking = {
    hostName = "lb1"; domain = "infra.ix-api.net"; 
    
    # Private network config
    interfaces.ens7 = {
       mtu = 1450;
       ipv4.addresses = [{address = "10.0.0.5"; prefixLength = 16; }];
    };
    firewall.trustedInterfaces = ["ens7"];
  };

  # This machine is a loadbalancer 
  machine = {
    # Loadbalancer configuration
    load-balancer.enable = true;
    load-balancer.vhosts = {
      # Public Website
      "www.ix-api.net".upstreams = [
          "10.0.0.3:9001"
          "10.0.0.4:9001"
      ];
      "ix-api.net".upstreams = [
          "10.0.0.3:9001"
          "10.0.0.4:9001"
      ];

      # Docs (live and staging)
      "docs.ix-api.net".upstreams = [
          "10.0.0.4:8001"
          # "10.0.0.3:8001"
      ];

      "docs-staging.ix-api.net".upstreams = [
          "10.0.0.3:8002"
          "10.0.0.4:8002"
      ];

      # Monitoring
      "grafana.infra.ix-api.net".upstreams = [
          "10.0.0.4:7200" 
      ];
    };

    monitoring.metrics.enable    = true;
    monitoring.metrics.collector = "http://10.0.0.4:8428";
  };

  # Additional VHost config, for errors.ix-api.net
  services.nginx.virtualHosts = {
    "errors.ix-api.net" = {
      enableACME = true;
      forceSSL   = true;

      # This is using the same upstream as docs,
      # however we differ in redirecting and proxies
      locations = {
        "/"    = { return = "302 /v2/"; };
        "/v2/" = {
            proxyPass = "http://lb-docs-staging.ix-api.net";
            proxyWebsockets = true;
            extraConfig = ''
              rewrite /v2/(.*) /v2/problems/$1 break;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header Host $host;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
        };
        "/v1/" = {
            proxyPass = "http://lb-docs-staging.ix-api.net";
            proxyWebsockets = true;
            extraConfig = ''
              rewrite /v1/(.*) /v1/problems/$1 break;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header Host $host;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
        };
      };
    };
  };

  users.motd = builtins.readFile ./motd;
}
