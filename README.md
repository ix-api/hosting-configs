
# IX-API Hosting Configurations

NixOS allows us to declarative describe the
state of a (virtual) machine. This includes everything from
configuration to installed packages.

This description is separated into `machine/` and
specific `hosts/` configurations.

## `machine/` configuration

The `machine/` configuration providing modules for
describing the "role" of a `container-host`,
`load-balancer` or `monitoring`, etc.
These roles are parameterized and will be applied
according to the host specific config declarations.

Also common machine wide configuration is stored here.
Admin-Users are declared in the `admins.nix` file,
`vim.nix` provides small tweaks to the system's vimrc.

#### Admin-Users

Admin users are declared in the `machine/admins.nix` file.
This is just an attribute set with usernames and a public key.
(Also make sure to add the user to the group `wheel` to allow
using `sudo`.)

### `machine.container-host`

The container host is using **docker** and **systemd** to spawn
containers with e.g. the website or schema documentation.

The following containers are provided:

```nix
machine.container-host = {
    enable = true;  # Enable container host role
    
    # Containers
    website.enable = true;      # Enable the website container
    website.listen = "<internal ip1>:9001";

    docs.live.enable = true;    # Enable the docs-live container
    docs.live.listen = "<internal ip1>:8001";

    docs.staging.enable = true; # Enable the docs-staging container
    docs.staging.listen = "<internal ip1>:8002";
};
```

#### CI and Updates

The containers are configured to always pull. Updating to the
newest container build is done through restarting the services:

    systemctl restart docker-ix-api-website
    systemctl restart docker-docs-live
    systemctl restart docker-docs-staging

This can be done through CI: The container host role spins up
a nginx + let's encrypt to provide `webtriggers` using a public key
token, which can be added to the gitlab ci environment.


### `machine.load-balancer`

The load-balancer module sets up nginx vhosts and configures
upstreams.
**Let's Encrpyt** is used for obtaining TLS certificates.

The following options are defined:

```nix
machine.load-balancer = {
    enable = true; # Enable the load balancer / nginx

    # Vhosts / Reverse Proxies
    "host.domain.tld".upstreams = [
        "<internal ip1>:9001"
        "<internal ip2>:9001"
        "<internal ip3>:9001"
    ];
};
```

The above example will configure nginx to serve the vhost
`host.domain.tld` and reverse proxies the upstreams from internal ip 1-3.


### `machine.monitoring`

This module sets up monitoring and metrics collection
using **grafana**, **victoriametrics** and **telegraf**.

The following options are defined:

```nix
# VictoriaMetrics 
machine.monitoring.collector = {
    enable = true;            # Enable victoriametrics on the host
    listen = "<addr>:<port>"; # Listen on this ip / port
};

# Grafana
machine.monitoring.grafana = {
    enable = true;     # Enable the grafana service
    addr   = "<addr>";
    port   = 7200;
};

# Telegraf
machine.monitoring.metrics = {
    enable    = true;      # Enable metrics collection
    collector = "<addr>";  # Send collected metrics here
};

```

## `hosts/` configurations

Each host has its own specific configuration in
`hosts/<fqdn>` which is imported in the toplevel 
`nixos/configuration.nix`.

The host specific configuration declares how the
machine is realized.

To create a container host with monitoring and a nginx on
the same machine, your host config would look something
like this:

```nix
{
  machine = {
    container-host.enable = true;
    container-host = {
      website      = { enable = true; listen = "127.0.0.1:9001"; };
    };
    
    load-balancer.enable = true;
    load-balancer.vhosts = {
      "ix-api.net".upstreams = [
          "127.0.0.1:9001"
      ];

    monitoring = {
      metrics.enable    = true;
      metrics.collector = "http://<victoriametrics/influxdb>:8428";
    };
  };
}

```

