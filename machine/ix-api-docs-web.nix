{config, lib, pkgs, ...}:
let
  docs = builtins.fetchGit {
    url = "https://gitlab.com/ix-api/ix-api-docs-web";
    ref = "refs/tags/1.3.4";
  };
in
{
  imports = [ (import "${docs}/nixos") ];
  config = {
    ix-api-docs-web = {};
  };
}

