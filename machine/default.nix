{
  imports = [
    ./vpc.nix
    ./packages.nix
    ./auto-upgrade.nix
    ./vim.nix
    ./admins.nix
    ./webtrigger.nix
    ./webmon.nix
    ./ix-api-docs-web.nix

    ./monitoring
    ./load-balancer
    ./container-host
    ./mailer
  ];

  # System configuration: Setup sudo and sudoers
  security.sudo.wheelNeedsPassword = false;
  nix.settings.trusted-users = [ "@wheel" ];

  # Disable password logins over ssh and configure
  # a nice looking banner.
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;

      # Mitigation: CVE-2024-6387
      # https://www.openwall.com/lists/oss-security/2024/07/01/3
      LoginGraceTime = 0; 
    };
  };

  # We enable let's encrypt
  security.acme = {
    defaults = {
      email = "annika+ixapi@hannig.cc";

      # For now use the staging server
      # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
    };

    acceptTerms = true;
  };
}
