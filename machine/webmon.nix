{config, pkgs, ...}:
let
  webmon = builtins.fetchGit {
    url = "https://gitlab.com/ix-api/webmon";
    ref = "refs/tags/v1.0.4";
  };
in
{
  imports = [ (import "${webmon}/nixos") ];
}
