{config, pkgs, lib, ...}:
let
  cfg = config.machine.vpc;
  options = with lib; {
    enable = mkEnableOption "VPC";
    interface = mkOption {
      type = types.str;
    };
    addr4 = mkOption {
      type = types.str;
    };
  };
in
{
  options.machine.vpc = options;
  config = lib.mkIf cfg.enable {
    networking = {
      interfaces."${cfg.interface}" = {
        mtu = 1450;
        ipv4 = {
          addresses = [
            {
              address = cfg.addr4;
              prefixLength = 16;
            }
          ];
        };
      };

      # Allow all internal traffic
      firewall.trustedInterfaces = [ cfg.interface ];
    };
  };
}
