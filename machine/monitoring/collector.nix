{config, pkgs, lib, ...}:
let
  cfg = config.machine.monitoring.collector;

  options = with lib; {
    enable = mkEnableOption "enable metrics collector";
    listen = mkOption {
      type = with types; nullOr str;
      description = "listen address and port";
      example = "10.1.2.3:8428";
      default = null;
    };
  };
in
{
  options.machine.monitoring.collector = options;

  config = lib.mkIf cfg.enable {
    # VictoriaMetrics
    services.victoriametrics = {
      enable = true;
      listenAddress = cfg.listen;
      retentionPeriod = 6; # month
      extraOptions = [];
    };

    networking.firewall.allowedTCPPorts = [80 443];
  };
}
