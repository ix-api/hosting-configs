{config, lib, pkgs, ...}:
let
    cfg = config.machine.monitoring.webmon;
    options = with lib; with types; {
        enable = mkEnableOption "enable active website monitoring"; 
    };

    # Metrics scraping
    scrapeConfig = with pkgs; writeTextFile {
        name = "scrape-webmon.yml";
        text = ''
global:
  scrape_interval: "1m"

scrape_configs:
  - job_name: "webmon"
    metrics_path: "/api/v1/metrics"
    static_configs:
      - targets: [ "localhost:22022" ]

'';
    };
in
{
    options.machine.monitoring.webmon = options;
    config = with lib; mkIf cfg.enable {
        services.webmon = {
            enable = true;
            urls = [
                "https://ix-api.net/"
                "https://www.ix-api.net/"
                "https://docs.ix-api.net/"
                "https://docs.ix-api.net/v2/schema.json"
                "https://docs.ix-api.net/v2/schema.yml"
                "https://docs-staging.ix-api.net/"
                "https://docs-staging.ix-api.net/v2/ix-api-latest.json"
                "https://docs-staging.ix-api.net/v2/ix-api-latest.yml"
                "https://errors.ix-api.net/v2/"
            ];
        };  

        services.victoriametrics.extraOptions = [
            "-promscrape.config=${scrapeConfig}"
        ];
    };
}
