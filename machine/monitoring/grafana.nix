{config, pkgs, lib, ...}:
let
  cfg = config.machine.monitoring.grafana;
  collectorAddr = config.machine.monitoring.collector.listen;

  options = with lib; with types; {
    enable = mkEnableOption "enable grafana";
    addr = mkOption {
      type        = nullOr str;
      description = "listen address";
      example     = "10.1.2.3";
      default     = null;
    };
    port = mkOption {
      type        = nullOr int;
      description = "listen port";
      default     = null;
    };
  };
in
{
  options.machine.monitoring.grafana = options;

  config = lib.mkIf cfg.enable {
    # Grafana
    services.grafana = {
      enable = true;
      settings = {
        server = {
          domain = config.networking.domain;
          http_port = cfg.port;
          http_addr = cfg.addr;
        };

        smtp = (import /secrets/grafana-smtp.nix);
      };
      
      # Runtime / State
      provision = {
        enable = true;
        datasources.settings = {
          datasources = [
            { 
              name = "victoriametrics@local";
              type = "prometheus";
              url  = "http://${collectorAddr}";
              isDefault = true;
            }
          ];
        };
        dashboards = {
          path = ./dashboards;
        };
      };
    };
  };
}
