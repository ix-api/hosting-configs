{config, lib, pkgs, ...}:
let
    cfg = config.machine.monitoring.metrics;

    # Options
    options = {
        enable = lib.mkEnableOption "system metrics monitoring";
        collector = with lib; mkOption {
          type        = types.str;
          default     = "http://localhost:8428";
          description = ''
            Send metrics to this server.
            Example http://[::1]:8428
          '';
        };
        extraInputs = with lib; mkOption {
          type    = types.attrs;
          default = {};
          description = "Extra inputs to be merged";
        };
    };

    defaultInputs = {
      cpu    = { percpu = true; totalcpu = true; };
      mem    = {};
      disk   = { ignore_fs = ["tmpfs" "devtmpfs"]; };
      diskio = {};
      kernel = {};
      processes = {};
      swap   = {};
      system = {};
      net    = {};
      netstat = {};
      interrupts = {};
      linux_sysctl_fs = {};
      systemd_units = {
        unittype = "service,timer";
      };
    };
in
{
    options.machine.monitoring.metrics = options;

    # Implementation
    config = lib.mkIf cfg.enable {
      # Self monitoring using telegraf and node exporter
      services.telegraf = {
        enable = true;
    
        extraConfig = {
          agent = {
            interval = "10s";
            hostname = "${config.networking.hostName}.${config.networking.domain}";
          };
          inputs = defaultInputs // cfg.extraInputs;
          outputs = {
            influxdb = {
              database = config.networking.domain;
              urls = [ cfg.collector ]; 
            };
          };
        };
      };
    };
}
