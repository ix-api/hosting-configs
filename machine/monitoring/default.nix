{
  imports = [ 
    ./collector.nix
    ./webmon.nix
    ./metrics.nix
    ./grafana.nix
  ];
}
