{config, pkgs, ...}:
{
    # Enable autoupgrades
    system.autoUpgrade = {
        enable = true;
        allowReboot = true;
    };

    # Also collect garbage, otherwise the harddrive will
    # run out of inodes faster than you can imagine.
    systemd = {
        services."collect-garbage" = {
            script = ''
                /run/current-system/sw/bin/nix-collect-garbage -d
            '';
            serviceConfig = {
                Type = "oneshot";
                User = "root";
            };
        };

        # Run the unit daily and 30 minutes after startup
        timers."collect-garbage" = {
            wantedBy = [ "timers.target" ];
            timerConfig = {
                Unit = "collect-garbage.service";
                OnBootSec = "30m";
                OnUnitActiveSec = "1d";
            };
        };
    };    
}
