{config, pkgs, lib, ...}:
let
  cfg = config.machine.container-host.website;
  containersCfg = config.virtualisation.oci-containers;

  container = "ix-api-website";

  restartTrigger = pkgs.writeScript "restart" ''
     systemctl restart ${containersCfg.backend}-${container}
  '';
  
  serverName = with config.networking; "${hostName}.${domain}";

  # Configuration
  options = with lib; {
    enable = lib.mkEnableOption "host the public website";
    listen = mkOption {
      type = types.str;
      description = ''
        the listen address of the service
      '';
    };
  };

in
{
  options.machine.container-host.website = options;

  # Implementation
  config = lib.mkIf cfg.enable {
    # Configure container
    virtualisation = {
      oci-containers = { 
        containers."ix-api-website" = {
          image = "registry.gitlab.com/ix-api/ix-api-website/live/ix-api-website:latest";
          ports = ["${cfg.listen}:80"];
          autoStart = true;
        };
      };
    };

    # When restarting, make sure we pull the new image
    # --pull=always is not yet available in this docker version.
    systemd.services."${containersCfg.backend}-ix-api-website".preStart = ''
      ${containersCfg.backend} pull ${containersCfg.containers."ix-api-website".image}
    '';

    # Restart trigger
    users.users.restart-website = {
      isSystemUser = true;
      group = "webtrigger";
    };
        
    security.sudo.extraRules = [ {
      users = [ "restart-website" ];
      runAs = "root";
      commands = [ {
        command = "${restartTrigger}";
        options = [ "NOPASSWD" "SETENV" ];
      } ];
    } ];
    
    services.webtrigger.enable = true;
    services.webtrigger.webtriggers.restart-website = {
      listen = "127.0.0.1:7203";
      pubkey = "ch1-OmniMACkToXup07luAT4Hgb-Tc__ih5lCpLHzTE=";
      cmd    = "/run/wrappers/bin/sudo ${restartTrigger}";
      user   = "restart-website";
    };

    # Configure nginx vhost
    services.nginx.virtualHosts = {
      ${serverName} = {
         forceSSL = true;
         enableACME = true;
         locations."/_webtrigger/website/restart" = {
           proxyPass = "http://127.0.0.1:7203";
         };
      };
    };

  };
}
