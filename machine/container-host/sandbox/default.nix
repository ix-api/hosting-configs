{config, pkgs, lib, ...}:
let
    cfg = config.machine.container-host.sandbox;
    options = with lib; {
        enable = mkEnableOption "enable the hosted sandbox";
        fqdn = mkOption {
            type = types.str;
            description = "fqdn of the hosted sandbox";
        };
        listen = mkOption {
            type = types.str;
            description = "listen port";
        };
    };

    # Secret
    secret = builtins.readFile "/secrets/ix-api-sandbox.secret";

    # Database Config
    pgDatabase = "ixapisandboxnext";
    pgUser = {
        name = pgDatabase;
        ensureDBOwnership = true;
    };

    pgService = {
        enable = true;
        package = pkgs.postgresql_16_jit;
        authentication = pkgs.lib.mkOverride 10 ''
          local all all trust
        '';
        ensureUsers = [ pgUser ];
        ensureDatabases = [ pgDatabase ];
    };

    image = "registry.gitlab.com/ix-api/ix-api-sandbox-v2/staging/ix-api-sandbox-v2:latest";

    # Sandbox
    env = {
     IXAPI_SANDBOX_DB_NAME = pgDatabase;
     IXAPI_SANDBOX_DB_USER = pgDatabase;
     IXAPI_SANDBOX_DB_PASSWORD = secret;
     IXAPI_SANDBOX_DB_HOST = "/var/run/postgresql";

     IXAPI_SANDBOX_FQDN = cfg.fqdn;

     IXAPI_SANDBOX_SECRET = secret;
     IXAPI_SANDBOX_DEBUG = "false";
    };

    mkSandbox = cmd: {
      inherit image cmd;
      volumes = [
        "/var/run/postgresql:/var/run/postgresql"
      ];
      environment = env; 
    };

    # Systemd
    sdServices = {
      # One-Shot systemd unit to pull the image
      "docker-ix-api-sandbox-v2-pull" = {
         description = "Pull ix-api-sandbox-v2 image";
         wantedBy = [];
         serviceConfig = {
           ExecStart = "${pkgs.docker}/bin/docker pull ${image}";
           Type = "oneshot";
         };
      };

      # Configure runserver wants postgresql
      "docker-ix-api-sandbox-v2" = {
        after = [ "postgresql.service" ];
      };

      # Configure migrate as oneshot
      "docker-ix-api-sandbox-v2-migrate" = {
         serviceConfig = {
           Type = "oneshot";
           Restart = lib.mkForce "no";
         };
      };

      # Shell needs to be redirected to a socket
      "docker-ix-api-sandbox-v2-shell" = {
          serviceConfig = {
            Type = "oneshot";
            Restart = lib.mkForce "no";
          };
      }; 
    };

    # Config
    mkConfig = enable: lib.mkIf enable {
     # Database 
     services.postgresql = pgService;

     # Containers
     virtualisation.oci-containers.containers = {
        "ix-api-sandbox-v2" = mkSandbox ["runserver" "0.0.0.0:8000"] // {
          ports = [ "${cfg.listen}:8000" ];
        };
        "ix-api-sandbox-v2-migrate" = (mkSandbox ["migrate"]) // {
          autoStart = false;
        };
        "ix-api-sandbox-v2-shell" = (mkSandbox ["remoteshell"]) // {
          ports = [ "127.0.0.1:18000:18000" ];
          autoStart = false;
        };
        "ix-api-sandbox-v2-state-provider" = (mkSandbox ["run_state_provider"]);
     };

     # Systemd units
     systemd.services = sdServices;
  };
in
{
   options.machine.container-host.sandbox = options;
   config = mkConfig cfg.enable;
}

