{config, pkgs, lib, ...}: 
let
  options = with lib; {
    enable = mkEnableOption "enable container-host role";
  };

  cfg = config.machine.container-host;
in
{
  options.machine.container-host = options;

  #
  # IX-API Services: All services are containers,
  # some are exposed through an nginx reverse proxy.
  #
  imports = [
    ./docs
    ./website
    ./sandbox
  ];

  config = lib.mkIf cfg.enable {
    # Configure virtualisation: We are using docker,
    # with systemd to orchestrate the containers.
    virtualisation = {
      docker.enable = true;
      oci-containers.backend = "docker";
    }; 

    # We will need a working nginx
    services.nginx.enable = true;    
    
    # Open firewall for http(s)
    networking.firewall.allowedTCPPorts = [80 443];
  };
}
