{config, lib, pkgs, ...}:
let
   cfg  = config.machine.container-host.docs.preview;
   opts = with lib; with types; {
     enable = mkEnableOption "docs: preview service";
     listen = mkOption {
      type = str;
      description = ''
        listen on this port
      '';
      default = "127.0.0.1:8020";
     };
   };

   restartScript = pkgs.writeScript "restart" ''
     systemctl restart ix-api-docs-preview
   '';
in
{
  options.machine.container-host.docs.preview = opts;
  config = lib.mkIf cfg.enable {

    # Enable docs builder on the machine
    ix-api-docs-web.preview = {
      listen = cfg.listen;
      buildLatest = false;
    };

    # Restart trigger
    systemd.paths.ix-api-docs-preview = {
      wantedBy = ["multi-user.target"];
      pathConfig = {
        PathChanged = "/var/lib/ix-api-docs-preview";
        Unit = "ix-api-docs-preview.service";
        TriggerLimitIntervalSec = 1;
        TriggerLimitBurst = 1;
        MakeDirectory = true;
      };
    };

  };
}
