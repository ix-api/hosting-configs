{config, lib, pkgs, ...}:
let
   cfg           = config.machine.container-host.docs.staging;
   containersCfg = config.virtualisation.oci-containers;
   container     = "docs-staging";

   serverName = with config.networking; "${hostName}.${domain}";

   restartTrigger = pkgs.writeScript "restart" ''
      systemctl restart ${containersCfg.backend}-${container}
   '';

   options = with lib; {
     enable = mkEnableOption "docs: live service";
     listen = mkOption {
       type = types.str;
       description = ''
         the listen address of the service
       '';
     };
   };
in 
{
    # Options for IX API Docs Service
    options.machine.container-host.docs.staging = options;

    # Implementation
    config = lib.mkIf cfg.enable {

      # Configure container
      virtualisation = {
        oci-containers = { 
          containers = {
            ${container} = {
              image = "registry.gitlab.com/ix-api/ix-api-schema/staging/ix-api-schema:latest";
              cmd = ["serve" "0.0.0.0:8080"];
              ports = ["${cfg.listen}:8080"];
              autoStart = true;
            };
          };
        };
      };

      # When restarting, make sure we pull the new image
      # --pull=always is not yet available in this docker version.
      systemd.services."${containersCfg.backend}-${container}".preStart = ''
        ${containersCfg.backend} pull ${containersCfg.containers.${container}.image}
      '';

      # Restart trigger:
      # Enable webtrigger for restarting the container, with sudo
      users.users.restart-docs-staging.isSystemUser = true;
      users.users.restart-docs-staging.group = "webtrigger";
      security.sudo.extraRules = [
        {
            users = [ "restart-docs-staging" ];
            runAs = "root";
            commands = [ 
              { command = "${restartTrigger}";
                options = [ "NOPASSWD" "SETENV" ];
              }
            ];
        }
      ];
        
      services.webtrigger.enable = true;
      services.webtrigger.webtriggers.restart-docs-staging = {
        listen = "127.0.0.1:7201";                              
        pubkey = "fGHLUe32XEttTGkURVvwTxDPVo6yeJ42KMHfyNkS3eA=";
        cmd    = "/run/wrappers/bin/sudo ${restartTrigger}";    
        user   = "restart-docs-staging";                                 
      };

      # Configure nginx vhost
      services.nginx.virtualHosts = {
        ${serverName} = {
           forceSSL = true;
           enableACME = true;

           locations."/_webtrigger/docs-staging/restart" = {
             proxyPass = "http://127.0.0.1:7201";
           };
        };
      };
    };
}
