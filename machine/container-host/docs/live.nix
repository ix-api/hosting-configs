{config, lib, pkgs, ...}:
let
   cfg  = config.machine.container-host.docs.live;
   opts = with lib; with types; {
     enable = mkEnableOption "docs: live service";
     listen = mkOption {
      type = str;
      description = ''
        listen on this port
      '';
      default = "8001";
     };
   };

   serverName = with config.networking; "${hostName}.${domain}";

   restartScript = pkgs.writeScript "restart" ''
     systemctl restart ix-api-docs-live
   '';
in
{
  options.machine.container-host.docs.live = opts;
  config = lib.mkIf cfg.enable {

    # Enable docs builder on the machine
    ix-api-docs-web.live = {
      listen = cfg.listen;
    };

    # Restart trigger
    users.users.restart-docs-live = {
      isSystemUser = true;
      group = "webtrigger";
    };
    security.sudo.extraRules = [ {
      users = [ "restart-docs-live" ];
      runAs = "root";
      commands = [ {
        command = "${restartScript}";
        options = [ "NOPASSWD" "SETENV" ];
      } ];
    } ];
    services.webtrigger.enable = true;
    services.webtrigger.webtriggers.restart-docs-live = {
      listen = "127.0.0.1:7202";
      pubkey = "fGHLUe32XEttTGkURVvwTxDPVo6yeJ42KMHfyNkS3eA=";
      cmd    = "/run/wrappers/bin/sudo ${restartScript}";
      user   = "restart-docs-live";
    };

    # Configure nginx vhost for serving the webtrigger
    services.nginx.virtualHosts = {
      ${serverName} = {
         forceSSL = true;
         enableACME = true;
         locations."/_webtrigger/docs-live/restart" = {
           proxyPass = "http://127.0.0.1:7202";
         };
      };
    };
  };
}
