import sys
import email
import subprocess
from argparse import ArgumentParser
from email.utils import parseaddr, formataddr


def parse_args():
    """Parse cli arguments"""
    parser = ArgumentParser()
    parser.add_argument(
        "-a", "--mailbox", help="group mail address", required=True)
    parser.add_argument(
        "-t", "--recipients-filename",
        help="file with all forward targets", required=True)
    parser.add_argument(
        "subject", nargs="+",
        help="prepend subject")

    return parser.parse_args()


def sendmail(sender, receiver, data):
    """Invoke sendmail"""
    s = subprocess.Popen([
        "/run/wrappers/bin/sendmail",
        "-G", "-i", "-f",
        sender, "--", receiver,
    ], stdin=subprocess.PIPE)
    s.communicate(input=data)


def set_header(msg, header, value):
    """Set the header"""
    del msg[header]
    msg[header] = value


def forward_email(mailbox, subject, recipients, mail):
    """On receive mail"""
    msg = email.message_from_string(mail)
    msg_from = msg["From"]
    from_name, _ = parseaddr(msg_from)

    set_header(msg, "From", formataddr((from_name, mailbox)))
    set_header(msg, "Subject",
               f"[{subject}] " + msg.get("Subject", "{empty subject}"))
    set_header(msg, "Reply-To", msg_from)

    # Send message to all recipients
    for recv in recipients:
        sendmail(mailbox, recv, msg.as_bytes())


def load_recipients(filename):
    """Load recipients from file"""
    recipients = []
    with open(filename, "r") as f:
        recipients = f.readlines()

    return [r.strip() for r in recipients if r != "\n"]


def main(args):
    """Read and rewrite incoming mail"""
    subject = " ".join(args.subject)
    recipients = load_recipients(args.recipients_filename)
    mail = sys.stdin.read()
    forward_email(args.mailbox, subject, recipients, mail)

    return 0


if __name__ == "__main__":
    """Rewrite Mail Subject"""
    args = parse_args()
    try:
        sys.exit(main(args))
    except Exception as e:
        print(e)
        sys.exit(-1)
