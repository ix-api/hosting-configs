{config, lib, ...}:
let
    cfg = config.machine.mailer;
    opts = with lib; with types; {
        enable = mkEnableOption "enable mailer";
    };
in
{
    imports = [ ./postfix.nix ./mailman.nix ];
    options.machine.mailer = opts;
}
