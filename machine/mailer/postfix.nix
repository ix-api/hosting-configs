{config, pkgs, lib, ...}:
let
    cfg = config.machine.mailer;

    acmeRoot = "/var/lib/acme/acme-challenge";
    mailFQDN = "service1.infra.ix-api.net";

    # Team Mailbox
    teamMailbox = "team@ix-api.net";
    teamSubject = "IX-API Team";
    teamRecipients = [
        "annika@hannig.cc"
        "sebastian.spies@de-cix.net"
        "benjamin.buettrich@de-cix.net"
        "thomas.king@de-cix.net"
        "dant@linx.net"
        "richard@linx.net"
        "riccardo@linx.net"
        "richardg@linx.net"
        "stavros.konstantaras@ams-ix.net"
        "petros.papadopoulos@ams-ix.net"
        "steven.bakker@ams-ix.net"
    ];
    
    # Admin Mailbox
    adminSubject = "IX-API Admin";
    adminMailbox = "admin@ix-api.net";
    adminRecipients = [
        "annika@hannig.cc"
    ];

    # Mail transport
    mkForwardTransport = mailbox: recipients: subject:
        with pkgs; with lib;
        let
            transport = writers.writePython3
                "forward.py" {} ./scripts/forward_mail.py;
            recipientsFile = writeText
                "recipients"
                (strings.concatStringsSep "\n" recipients);
        in {
            command = "pipe";
            privileged = true;
            args = [
                "user=mailfilter"
                "argv=${transport} -a ${mailbox} -t ${recipientsFile} ${subject}"
            ];
        }; 
   
in
{
  config = lib.mkIf cfg.enable {
    # Open firewall ports: SMTP SMTPS HTTP HTTPS
    networking.firewall.allowedTCPPorts = [ 25 587 80 443 ];

    # Add mailfilter user
    users.users.mailfilter = {
      group = "mailfilter";
      isSystemUser = true;
      home = "/var/mailfilter";
      createHome = true;
    };
    users.groups.mailfilter = {};

    # We need python for the forward script
    environment.systemPackages = with pkgs; [ python3 ];

    # SMTPD TLS certificate
    services.nginx = {
        enable = true;
        virtualHosts."${mailFQDN}" = {
            serverName = mailFQDN;
            forceSSL = true;
            enableACME = true;
            acmeRoot = acmeRoot;
        };
    }; 

    # Configure postfix 
    services.postfix = {
      enable = true;
      enableSmtp = false; # override
      enableSubmission = true;
      domain = "ix-api.net";
      origin = "ix-api.net";
      destination = ["localhost" "ix-api.net"];
      relayHost = "email-smtp.eu-west-1.amazonaws.com";
      relayPort = 587;
      config = {
         # Outbound via relay
         smtp_sasl_auth_enable = "yes";
         smtp_sasl_security_options = "noanonymous";
         smtp_sasl_password_maps = "hash:/secrets/postfix_sasl_passwd";
         smtp_use_tls = "yes";
         smtp_tls_security_level = "encrypt";
         smtp_tls_note_starttls_offer = "yes";

         # Inbound
         smtpd_use_tls = true;
         smtpd_tls_security_level = "may";
         smtpd_tls_cert_file = "/var/lib/acme/${mailFQDN}/fullchain.pem";
         smtpd_tls_key_file = "/var/lib/acme/${mailFQDN}/key.pem";    
      };
      masterConfig = {
        relay = {
          command = "smtp";
        };
        smtp_inet = {
          name = "smtp";
          type = "inet";
          private = false;
          command = "smtpd";
        };
        smtp = {};
        forward_team = mkForwardTransport
            teamMailbox
            teamRecipients
            teamSubject;
        forward_admin = mkForwardTransport
            adminMailbox
            adminRecipients
            adminSubject;
      };

      transport = ''
        ${teamMailbox} forward_team:
        ${adminMailbox} forward_admin:
      '';
      localRecipients = [ teamMailbox adminMailbox ];
    
      # Aliases
      rootAlias = "admin@ix-api.net";
    };
  };
}
