{config, pkgs, lib, ...}:
#
# Mailman Configuration
# This is based on nixos.wiki/wiki/Mailman
#
let
  cfg = config.machine.mailer;
  webHost = "lists.ix-api.net";
in
{
  config = lib.mkIf cfg.enable { 
    # Additional postfix configuration
    services.postfix = {
      relayDomains = ["hash:/var/lib/mailman/data/postfix_domains"];
      config = {
        transport_maps = ["hash:/var/lib/mailman/data/postfix_lmtp"];
        local_recipient_maps = ["hash:/var/lib/mailman/data/postfix_lmtp"];
      };
    };

    # Mailman and HyperKitty
    services.mailman = {
      enable = true;
      siteOwner = "admin@ix-api.net";
      webHosts = [webHost]; 
      serve.enable = true; # Nginx / UWSGI
      hyperkitty = {
        enable = true;
      };
    };

    # WebServer
    services.nginx = {
       virtualHosts.${webHost} = {
         enableACME = true;
         forceSSL = true;      
       };
    }; 
  };
}
