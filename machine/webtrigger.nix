{config, lib, pkgs, ...}: 
let
    webtrigger = builtins.fetchTarball {
        url = "https://gitlab.com/ahannig/webtrigger/-/archive/1.1.3/webtrigger-1.1.3.tar.gz";
        sha256 = "1gz0vhd1523r81ax9jpgnd7l2mb98vq8gy2wi90d8llq8asn5cx5";
    };
in 
{
    imports = [ (import "${webtrigger}/nixos") ];
    config = {
        users.groups.webtrigger = {};
    };
}
