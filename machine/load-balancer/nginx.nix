{config, pkgs, lib, ...}:
let

  cfg = config.machine.load-balancer;

  siteOptions = with lib; {
    upstreams = mkOption {
      type = with types; listOf str;
      description = "a list of upstreams";
      default = [];
    };
    locations = mkOption {
      type = types.attrs;
      description = "extra locations";
      default = {};
    };
  };

  options = with lib; with types; {
    enable = mkEnableOption "enable load balancer";
    vhosts = mkOption {
      type = attrsOf (submodule { options = siteOptions; });
      description = "configure the loadbalanced vhosts here";
    };
  };

  # make list of upstream servers
  mkUpstreamConfig = 
    upstreams:
      builtins.concatStringsSep "\n" (map (u: "server ${u};") upstreams);

  mkUpstreamConfigs = with lib; fqdn: opts:
      (nameValuePair "lb-${fqdn}" {
         extraConfig = mkUpstreamConfig opts.upstreams;
      });
    
  mkVhostConfig = with lib; fqdn: opts:
    {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/" = {
            proxyPass = "http://lb-${fqdn}";
            proxyWebsockets = true;
            extraConfig = ''
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header Host $host;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
          };
        } // opts.locations;
     };
 
in
{
  options.machine.load-balancer = options;
  
  # Implementation
  config = lib.mkIf cfg.enable {
    services.nginx.enable = true;
    networking.firewall.allowedTCPPorts = [ 80 443 ];

    # Configure nginx
    services.nginx = {
       upstreams    = with lib; mapAttrs' mkUpstreamConfigs cfg.vhosts;
       virtualHosts = with lib; mapAttrs mkVhostConfig cfg.vhosts;

       # Enable status for metrics collection
       statusPage = true;
    };

    # Collect metrics
    machine.monitoring.metrics.extraInputs.nginx = {
      urls = [ "http://localhost/nginx_status" ];
    };
  };
}

